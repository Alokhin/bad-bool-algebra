let variables = {};
let operations = {};

class Tree{
  constructor(){
    this.identifiers = new Set();
    this.variables = {};
  }
  static createTree(obj,identifiers = new Set(), isRoot = true){
    let tree;
    let defineType = (node)=>{
      if(node.operator){
        if(node.left && node.right) return 'BinaryExpression';
        if(node.argument) return 'UnaryExpression';
      }
      if(node.name) return 'Identifier';
      if(node.value) return 'Literal';
    }
    if(!obj) return;
    if(!obj.type) obj.type = defineType(obj);
    switch(obj.type) {
      case 'BinaryExpression':
        tree = new BinaryNode(obj.operator,
          Tree.createTree(obj.left, identifiers),
          Tree.createTree(obj.right, identifiers));
        break;
      case 'UnaryExpression':
        tree = new UnaryNode(obj.operator, Tree.createTree(obj.argument,identifiers));
        break;
      case 'Identifier':
        tree = new IdentifierNode(obj.name);
        identifiers.add(obj.name);
        break;
      case 'Literal':
        tree = new LiteralNode(obj.value);
        break;
    }
    tree.identifiers = identifiers;
    return tree;
  }
  get operator(){
    return operations[this._operator];
  }
  compute(variables){
    if(this.isIdentifier())
      return variables[this.name];
    if(this.isLiteral())
      return this.value;
    if(this.isBinary())
      return this.operator.func(this.left.compute(variables),
        this.right.compute(variables));
    if(this.isUnary())
      return this.operator.func(this.argument.compute(variables));
  }
  toTable(){
    let res = [];
    let vals = Table.values(this.identifiers.size);
    let variables = {};
    for(let val of vals){
      let j = 0;
      for(let variable of this.identifiers){
        variables[variable]=val[j++];
      }
      // console.log(variables);
      res.push(this.compute(variables)?1:0);
    }
    return(new Table(vals,res,this.identifiers));
  }
  toString(prec = 0){
    if(this.isBinary()) {
      let oprec = this.operator.prec || 0;
      let str = `${this.left.toString(oprec)} ${this._operator} ${this.right.toString(oprec)}`;
      if (oprec >= prec)
        return str;
      else return `(${str})`
    }
    else if(this.isUnary())
      return `${this._operator} ${this.argument.toString()}`;
    else if(this.isIdentifier())
      return this.name;
    else if(this.isLiteral())
      return this.value;
  }
  toFunction(){
    return (args) => {
      for(let v in args)
        this.variables[v] = args[v]
      return this.compute(this.variables);
    };
  }
  toFunctionWithArgs(){
    return (...args) => {
      let i = 0;
      for(let variable of this.identifiers){
        if(i >= args.length)
          break;
        this.variables[variable] = args[i++];
      }
      return this.compute(this.variables);
    };
  }
  isUnary(){
    return this instanceof UnaryNode;
  }
  isBinary(){
    return this instanceof BinaryNode;
  }
  isLiteral(){
    return this instanceof LiteralNode;
  }
  isIdentifier(){
    return this instanceof IdentifierNode;
  }
  isTerminal(){
    return this.isIdentifier() || this.isLiteral();
  }
}
class BinaryNode extends Tree{
  constructor(operator,left,right){
    super();
    this.type = 'BinaryExpression';
    this._operator = operator;
    this.left = left;
    this.right = right;
  }
}
class UnaryNode extends Tree{
  constructor(operator,argument){
    super();
    this.type = 'UnaryExpression';
    this._operator = operator;
    this.argument = argument;
  }
}
class IdentifierNode extends Tree{
  constructor(name){
    super();
    this.type = 'Identifier';
    this.name = name;
  }
}
class LiteralNode extends Tree{
  constructor(value){
    super();
    this.value = value;
  }
}

class Table {
  constructor(val, res, vars) {
    this.val = val || [];
    this.res = res || [];
    this.vars = vars || [];
  }

  row(i) {
    return [...this.val[i], this.res[i]]
  }

  static _generate_values(number) {
    let result = [],
      nth = (a, b) => a >> b & 1;
    for (let i = 0; i < Math.pow(2, number); i++) {
      let row = [];
      for (let j = number - 1; j >= 0; j--) {
        row.push(nth(i, j));
      }
      result.push(row);
    }
    return result;
  }

  static values(number) {
    if (!this._saved_values[number])
      this._saved_values[number] = this._generate_values(number);
    return this._saved_values[number].slice(); // cloning
  }

  toString() {
    let str = '';
    for (let v of this.vars) {
      str += v + ' '
    }
    str += '\n';
    for (let i = 0; i < this.val.length; i++) {
      str += this.val[i].join(' ') + ' | ' + this.res[i] + '\n';
    }
    return str;
  }

  toDNF() {
    let size = this.val.length;
    let vars = Array.from(this.vars);
    let addTree = (op,trees)=>{
      let res = new BinaryNode(op,trees[0]);
      for(let i = 1; i < trees.length-1; i++){
        res.right = trees[i];
        res = new BinaryNode(op,res)
      }
      res.right = trees[trees.length-1];
      return res;
    }
    let disjs = [];
    for (let i = 0; i < size; i++) {
      let conj;
      if (this.res[i] != 0) {
        let stree = new BinaryNode('*');
        let foo = (i,j)=> {
          let val = new IdentifierNode(vars[j]);
          if(this.val[i][j] === 0)
            return new UnaryNode('~', val);
          return val;
        };
        let conjs = [];
        for (let j = 0; j < vars.length; j++) {
            conjs.push(foo(i,j))
          }
        conj = addTree('*',conjs);
        disjs.push(conj);
      }
    }
    return addTree('+',disjs);
  }

  toCNF() {
    let size = this.val.length;
    let vars = Array.from(this.vars);
    let addTree = (op,trees)=>{
      let res = new BinaryNode(op,trees[0]);
      for(let i = 1; i < trees.length-1; i++){
        res.right = trees[i];
        res = new BinaryNode(op,res)
      }
      res.right = trees[trees.length-1];
      return res;
    }
    let disjs = [];
    for (let i = 0; i < size; i++) {
      let conj;
      if (this.res[i] == 0) {
        let stree = new BinaryNode('*');
        let foo = (i,j)=> {
          let val = new IdentifierNode(vars[j]);
          if(this.val[i][j] === 0)
            return new UnaryNode('~', val);
          return val;
        };
        let conjs = [];
        for (let j = 0; j < vars.length; j++) {
          conjs.push(foo(i,j))
        }
        conj = addTree('+',conjs);
        disjs.push(conj);
      }
    }
    return addTree('*',disjs);
  }
}

let parser = require('jsep');

parser.removeAllBinaryOps();
parser.removeAllUnaryOps();
class Operation{
  constructor(name, symbol, func, prec = 0){
    this.name = name;
    this.symbol = symbol;
    this.func = func;
    this.prec = prec;
  }
}

const unaries = {},
  binaries = {};
const addUnaryOp = function (name, symbol, func) {
    let operation = new Operation(name, symbol, func);
    parser.addUnaryOp(symbol);
    unaries[symbol] = operation;
    return operation
  },
  addBinaryOp = function (name, symbol, func, prec) {
    let operation = new Operation(name, symbol, func,prec);
    parser.addBinaryOp(symbol, prec);
    binaries[symbol] = operation;
    return operation
  };
const addParsBinaryOp = function (name, symbol, rawFunc, prec) {
  let tree = Tree.createTree(parser(rawFunc));
  let operation = new Operation(name, symbol, tree.toFunctionWithArgs());
  operation.raw = true;
  operation.rawFunc = rawFunc;
  parser.addBinaryOp(symbol, prec);
  binaries[symbol] = operation;
  return operation
};
addUnaryOp('not','~',(x)=> !x);
addBinaryOp('and','*',(x,y)=> x && y, 10);
addBinaryOp('or','+',(x,y)=> x || y, 9);
addParsBinaryOp('imp','->','~x + y', 5);
addParsBinaryOp('eq','<->','x * y + ~x * ~y', 5);
addParsBinaryOp('xor','+|','x * ~y + ~x * y', 5);


Object.assign(operations,unaries,binaries);

Table._saved_values = {};
console.log(Table._saved_values);

exports.Tree = Tree;
exports.parser = parser;


