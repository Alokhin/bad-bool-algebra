let parser = require('jsep');
parser.removeAllBinaryOps();
parser.removeAllUnaryOps();
class Operation{
  constructor(name, symbol, func){
    this.name = name;
    this.symbol = symbol;
    this.func = func;
  }
}
class Tree{
  constructor(parent){
    this.parent = parent;
  }
  toFunction(args){
    return (a)=>{
      console.log(a);
      if (this.isBinary()) {
        return binaries[this._operator].func(null,this.left.toFunction.call(), this.right.toFunction.call());
      }
    }
  }
  match(tree,sys = {}, isRoot = false){
    this.parent = new Tree(parser('~x'));
    this.parent.argument = this;
    if (isRoot)
      sys.root = this;
    let result = true;
    let self = this;
    if (tree.isIdentifier()){
      sys[tree.name] = this;
      return true;
    }
    if(!(this.type == tree.type))
      return false;
    if(this.isBinary())
      return this._operator == tree._operator
          && this.left.match(tree.left,sys)
          && this.right.match(tree.right,sys);
    else if(this.isUnary())
      return this._operator == tree._operator
        && this.argument.match(tree.argument,sys);
    // else if(this.isIdentifier())
    //   return this.name == tree.name;
    else if(this.isLiteral())
      return this.value == tree.value;
  }
  test(tree){
    let sys = {};
    console.log(this.match(tree,sys));
    console.log(sys);
  }
  static change(tree, pattern) {
    let sys = {};
    //
    if (tree.match(pattern.from, sys, true)) {
      console.log(11111111);
      console.log(sys);
      tree.parent.argument = pattern.to;
      tr
    }
  }
  // *generateSequence(){
  //   yield 1;
  //   yield 2;
  //   }
  equals(tree){
    if (!(this.type == tree.type))
      return false;
    if(this.isBinary())
      return this._operator == tree._operator ;
        // &&
        // this.left.equals(tree.left) &&
        // this.right.equals(tree.right);
    else if(this.isUnary())
      return this._operator == tree._operator;
        // && this.argument.equals(tree.argument);
    else if(this.isIdentifier())
      return this.name == tree.name;
    else if(this.isLiteral())
      return this.value == tree.value;
  }
  isUnary(){
    return this.type == 'UnaryExpression';
  }
  isBinary(){
    return this.type == 'BinaryExpression';
  }
  isLiteral(){
    return this.type == 'Literal';
  }
  isIdentifier(){
    return this.type == 'Identifier';
  }
  isTerminal(){
    return this.isIdentifier() || this.isLiteral();
  }

  static createTree(obj,parent){
    if(!obj) return;
    let tree = new Tree(parent || null);
    tree.type = obj.type;
    switch(tree.type) {
      case 'BinaryExpression':
        tree.left = this.createTree(obj.left,obj);
        tree.right = this.createTree(obj.right,obj);
        tree._operator = obj._operator;
        break;
      case 'UnaryExpression':
        tree.argument = this.createTree(obj.argument,obj);
        tree._operator = obj._operator;
        break;
      case 'Identifier':
        tree.name = obj.name;
        break;
      case 'Literal':
        tree.value = obj.value;
        tree._raw = obj.raw;
        break;
    }
    return tree;
  }

}
const unaries = {},
  binaries = {};
const addUnaryOp = function (name, symbol, func) {
  let operation = new Operation(name, symbol, func);
  parser.addUnaryOp(symbol);
  unaries[name] = operation;
  return operation
},
  addBinaryOp = function (name, symbol, func, prec) {
    let operation = new Operation(name, symbol, func);
    parser.addBinaryOp(symbol, prec);
    binaries[symbol] = operation;
    return operation
  };
const addParsBinaryOp = function (name, symbol, func, prec) {
  let operation = new Operation(name, symbol);
  operation.raw = true;
  operation.rawFunc = func;
  parser.addBinaryOp(symbol, prec);
  binaries[symbol] = operation;
  return operation
};

addUnaryOp('not','~',(x)=>!x);
addBinaryOp('and','*',(x,y)=> x && y, 10);
addBinaryOp('or','+',(x,y)=> x || y, 9);
addParsBinaryOp('imp','->','!x + y', 5);
let tree = parser('x + y -> (x + ~ y)');
const execTree = function (tree) {
  let type = tree.type;
  if(type == 'Identifier' || type == 'Literal'){
    return
  }
  else if( type == 'UnaryExpression'){
    return
  }
}

// console.log(parser('x + y + ~ 1'));
let t = Tree.createTree(parser('~(x+y) * ~(y*~x)'));
console.log(t);
let pat = {
  from: Tree.createTree(parser('~x * ~y')),
  to : Tree.createTree(parser('~(x + y)'))
};
// console.log(pat);

// t.test(pat);
Tree.change(t,pat);
// console.log(binaries['+'].func(1,0));
// let t2 = Tree.createTree(parser('1 + 0'));
// console.log(t2.toFunction()(2));
// console.log(t.toFunction({x:'ds'}));