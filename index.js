let Tree = require('./tree').Tree;
let parser = require('./tree').parser;
let parsed = parser('~ x + x * ~ y + z');
let tree = Tree.createTree(parsed);


tree = Tree.createTree(parser('x*(y+~z)'))
let foo = tree.toFunctionWithArgs()
// console.log(tree.toString());

// console.log(tree.toTable().toString());
tree.toTable().toDNF();

const readline = require('readline');

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

rl.question('Function: \n', (answer) => {
  // TODO: Log the answer in a database
  let tree = Tree.createTree(parser(answer))
  console.log(tree.toTable().toString());
  console.log(`DNF: ${tree.toTable().toDNF().toString()}`);
  console.log(`CNF: ${tree.toTable().toCNF().toString()}`);

  rl.close();

});
